//
//  ViewController.swift
//  Swift Score Keeper 2
//
//  Created by Francisco Morales on 9/9/17.
//  Copyright © 2017 Francisco Morales. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITextFieldDelegate {
    
    @IBOutlet var team1Lbl: UILabel!
    @IBOutlet var team2Lbl: UILabel!

    @IBOutlet var team1Stepper: UIStepper!
    @IBOutlet var team2Stepper: UIStepper!
    
    @IBOutlet weak var team1TxtField: UITextField!
    @IBOutlet weak var team2TxtField: UITextField!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Part of the keyboard hiding code
        self.team1TxtField.delegate = self
        self.team2TxtField.delegate = self
        
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    //STEPPERS : Directing labels to change according to the values they have (1,2,3,...<=21)
    @IBAction func team1Stepper(_ sender: UIStepper) {
        let myString = String(format: "%.0f", team1Stepper.value)
        team1Lbl.text = myString
        
    }
    @IBAction func team2Stepper(_ sender: UIStepper) {
        let myString = String(format: "%.0f", team2Stepper.value)
        team2Lbl.text = myString

    }
    //RESET BUTTON : Resetting values in label & steppers on both side
    @IBAction func resetBtn(_ sender: UIButton) {
        team1Stepper.value = 0
        team1Lbl.text = "0"
        team2Stepper.value = 0
        team2Lbl.text = "0"
    }
    
    //HIDE KEYBOARD
    //Presses outside of keyboard
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    //Presses return key
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        team1TxtField.resignFirstResponder()
        team2TxtField.resignFirstResponder()
        return (true)
    }

}

